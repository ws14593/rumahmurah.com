<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if(!function_exists('home_page_news_block')){
	/**
	 * Return search tabs for index page
	 */
	function home_page_news_block(){
		$CI = &get_instance();
                $CI->load->model('news/news_model');
                $count = 4;
                $current_lang_id = 2;
                $data['current_date'] = date("Y-m-d H:i:s");
                $params = array(
                    "where"=>array("pg_news.status"=>1, "pg_news.id_lang"=>$current_lang_id, "pg_news_feeds.external"=>0), 
                    "or_where"=>array("pg_news.news_type"=>"feeds", "pg_news.scheduling <="=>$data['current_date'])     
                );
                #get news from feed#
                $rss = new DOMDocument();
                $rss->load('http://infoproperti.com/Bahasa/news/rss'); // Set the blog RSS feed url here
                $feed_limit = 4;
                $feed = array();
                foreach ($rss->getElementsByTagName('item') as $i=>$node) {
                        if ($i == $feed_limit) {
                            break;
                        }
                        $item = array ( 
                                'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
                                'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
                                'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
                                'date_add' => date("D, d-m-Y H:i:s", strtotime($node->getElementsByTagName('pubDate')->item(0)->nodeValue))
                                );
                        array_push($feed, $item);
                }
                $data['news_headline'] = $feed;
                #end news from feed#
                #$data['news_headline'] = $CI->news_model->get_news_block_list(1, $count, array("pg_news.date_add" => "DESC"), $params);				                
                $view = $CI->load->view('news/default/home_page_news_block', $data, true);
                return $view;

	}
}


