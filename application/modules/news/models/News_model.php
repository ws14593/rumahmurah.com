<?php
/**
* News main model
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ð¡Ñ€, 02 Ð°Ð¿Ñ€ 2010) $ $Author: kkashkova $
**/


class News_model extends CI_Model
{
	private $internal_news = array(
		'pg_news.id',
		'pg_news.gid',
		'pg_news.name',
		'pg_news.annotation',
		'pg_news.img',
		'pg_news.status',
		'pg_news.id_lang',
		'pg_news.news_type',
		'pg_news.date_add',
		'pg_news.feed_link',
		'pg_news.feed_id',
		'pg_news.feed_unique_id',
		'pg_news_feeds.external'
	);
        
	public function get_news_block_list($page=null, $items_on_page=null, $order_by=null, $params=array(), $filter_object_ids=null, $formated=true)
        {
                #print_r($params);exit;
		#MOD FOR EXTERNAL NEWS#
		$this->db->select(implode(", ", $this->internal_news));
		$this->db->from('pg_news');	
		$this->db->join('pg_news_feeds', 'pg_news.feed_id = pg_news_feeds.id', 'left');	
		#END OF EXTERNAL NEWS MOD#
                
		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->db->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->db->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->db->where($value);
			}
		}

		if(isset($filter_object_ids) && is_array($filter_object_ids) && count($filter_object_ids)){
			$this->db->where_in("id", $filter_object_ids);
		}

		if(is_array($order_by) && count($order_by)>0){
			foreach($order_by as $field => $dir){
				if(in_array($field, $this->internal_news)){
					$this->db->order_by($field." ".$dir);
				}
			}
		}
		#mod to put an alternative condition on a failed joint table to allow news#
		if(isset($params["or_where"]) && is_array($params["or_where"]) && count($params["or_where"])){
			$first=true;
			foreach($params["or_where"] as $field=>$value){
				if($first){
					$this->db->or_where($field, $value);
					$first=false;
				}
				else
				{	
					$this->db->where($field, $value);
				}
			}
		}
		#end of mod#
		if(!is_null($page) ){
			$page = intval($page)?intval($page):1;
			$this->db->limit($items_on_page, $items_on_page*($page-1));
		}
		$results = $this->db->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[] = $r;
			}
			return $data;
		}
		return array();
                
        }
}

