<?php
/**
* News main model
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ð¡Ñ€, 02 Ð°Ð¿Ñ€ 2010) $ $Author: kkashkova $
**/

define('GALLERY', 'pg_gallery');
define('LISTINGS', 'pg_listings');
define('RESIDENTIAL', 'pg_listings_residential');
define('COMMERCIAL', 'pg_listings_commercial');

class Listings_model extends CI_Model
{
	private $internal_listings = array(
		'pg_listings.id',
		'pg_listings.date_created',
		'pg_listings.headline',
		'pg_listings.price',
		'pg_listings.logo_image',
		'pg_listings.status',
		'pg_gallery.id AS gallery_id',
		'pg_gallery.file_name',
	);
        
	public function get_listings_block_list($page=null, $limit=null, $order_by=null, $params=array(), $filter_object_ids=null, $formated=true)
        {
		#MOD FOR EXTERNAL NEWS#
		$this->db->select(implode(", ", $this->internal_listings));
		$this->db->from(LISTINGS);
		$this->db->join(GALLERY, LISTINGS.'.id = '.GALLERY.'.object_id', 'left');	
		#END OF EXTERNAL NEWS MOD#
                
		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->db->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->db->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->db->where($value);
			}
		}

		if(isset($filter_object_ids) && is_array($filter_object_ids) && count($filter_object_ids)){
			$this->db->where_in("id", $filter_object_ids);
		}

		if(is_array($order_by) && count($order_by)>0){
			foreach($order_by as $field => $dir){
				if(in_array($field, $this->internal_listings)){
					$this->db->order_by($field." ".$dir);
				}
			}
		}
                
                $this->db->limit($limit);

		$results = $this->db->get()->result_array();
                //print_r($results); exit;
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[] = $r;
			}
                        
			return $data;
		}
		return array();
                
        }
}
