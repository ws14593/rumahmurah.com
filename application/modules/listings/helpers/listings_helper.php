<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if(!function_exists('home_page_listings_block')){
	/**
	 * Return search tabs for index page
	 */
	function home_page_listings_block(){
		$CI = &get_instance();
                $CI->load->model('listings/Listings_model');
                $limit = 10;
                $params = array(
                    "where" => array(
//                        LISTINGS.".status" => 1,
                        LISTINGS.".price <=" => 50000.0000,
                        LISTINGS.".logo_image !=" => '',
                        GALLERY.".sorter" => 1
                    )
                );
                $listings = $CI->Listings_model->get_listings_block_list(1, $limit, array(LISTINGS.".date_created" => "DESC"), $params);
                foreach($listings as $k => $v) {
                    $v['path'] = 'http://localhost/pakarpro.com/uploads/listing-photo/listings-photo/'.$v['id'].'/'.$v['gallery_id'].'/';
                    $v['media'] = $v['path'].'200_200-'.$v['file_name'];
                    
                    $data['listings'][] = $v;
                }

                $view = $CI->load->view('listings/default/home_page_listings_block', $data, true);
                
                return $view;

	}
}


