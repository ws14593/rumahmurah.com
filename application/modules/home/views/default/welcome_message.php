<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link href="<?php echo base_url('vendor/twbs/bootstrap/dist/css/bootstrap.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('css/home.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('plugins/owl-carousel/dist/owl.carousel.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('plugins/owl-carousel/dist/owl.theme.css'); ?>" rel="stylesheet">
    <style>
        #sliders .item img, #newest_listings .item img { display: block; width: 100%; height: auto; }
        #newest_listings .item { margin: 3px; }
    </style>
</head>
<body>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <span class="navbar-brand">Rumahmurah.com</span>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="menu">
                        <a href="#">Cari Rumah</a>
                    </li>
                    <li>
                        <a href="#">Pasang Iklan</a>
                    </li>
                    <li>
                        <a href="#">Tren Properti</a>
                    </li>
                    <li>
                        <a href="#">Kalkulator KPR</a>
                    </li>
                    <li>
                        <a class="btn btn-danger" href="#">Login</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        <!-- /.container -->
    </nav>

<div class="container-fluid">
    <div class="row">
        <div>
            <div id="sliders" class="owl-carousel owl-theme">
                <div class="item"><img src="<?php echo base_url('uploads/sliders/1/1600_440-6d7b10a740.jpg'); ?>" alt="6d7b10a740"></div>
                <div class="item"><img src="<?php echo base_url('uploads/sliders/2/1600_440-bb53142478.jpg'); ?>" alt="bb53142478"></div>
                <div class="item"><img src="<?php echo base_url('uploads/sliders/3/1600_440-d81e88ba53.jpg'); ?>" alt="d81e88ba53"></div>
                <div class="item"><img src="<?php echo base_url('uploads/sliders/4/1600_440-ac5dccb860.jpg'); ?>" alt="ac5dccb860"></div>
                <div class="item"><img src="<?php echo base_url('uploads/sliders/5/1600_440-07ddc13910.jpg'); ?>" alt="07ddc13910"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <?php echo $listings;?>
    </div>    
    <div class="news">
        <div class="row">
            <div style="max-width:1000px; width:100%; margin:0 auto;">
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10"><?php echo $news;?></div>
                <div class="hidden-xs hidden-sm col-md-3 col-lg-2">piosdjoiasjdoasjda</div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: green;">footer</div>
    </div>      
</div>
</body>
</html>

<!-- JS scripts are here -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.9/angular.min.js"></script>
<script src="<?php echo base_url('vendor/twbs/bootstrap/dist/js/bootstrap.js') ;?>"></script>
<script src="<?php echo base_url('plugins/owl-carousel/dist/owl.carousel.min.js') ;?>"></script>
<script>
    $(document).ready(function(){
        // for sliders
        $('#sliders').owlCarousel({
            autoplay: true,
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem:true,
            pagination: false
        });
        
        // for newest listings
        $("#newest_listings").owlCarousel({
            autoPlay: true,
            autoplayTimeout: 2000,
            items : 5,
            itemsDesktop : [1199,4],
            itemsDesktopSmall : [979,3],
            //navigation: true,
            //pagination: false
        });
    });
</script>
