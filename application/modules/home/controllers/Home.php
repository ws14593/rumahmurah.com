<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
        
    public $CI;
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    
        function __construct() {
		parent::__construct();
                $this->CI = &get_instance();
		$this->load->helper(array('form','xml'));
	}
        
	public function index()
	{
                //$xml = file_get_contents(APPPATH.'views/testing.xml');

                // load library
                //$this->load->library('Simplexml');

                //$xmlData = $this->xml_parser->xml_parse($xml);
                //$data['xmlData'] = $xmlData;
                //print_r($data['xmlData']); exit;
                
                $this->load->helper('news/news');
                $this->load->helper('listings/listings');
                $data['news'] = home_page_news_block();
                $data['listings'] = home_page_listings_block();
                #print_r($data);
		$this->load->view('default/welcome_message', $data);
	}
}
